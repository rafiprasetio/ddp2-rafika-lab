package movie;

public class Movie {

    private String judul; //judul Film
    private String genre; //genre film
    private int menit; //durasi Film
    private String rating; //rating Film (umum/remaja/dewasa)
    private String asal; //asal Film (lokal/impor)

    public Movie(String judul, String rating, int menit, String genre, String asal) { //constructor
        this.judul = judul;
        this.genre = genre;
        this.menit = menit;
        this.rating = rating;
        this.asal = asal;
    }

    public String getJudul(){ //getter judul movie
        return judul;
    }

    public String getGenre(){ //getter genre movie
        return genre;
    }

    public int getMenit() { //getter durasi movie
        return menit;
    }

    public String getAsal() { //getter asal movie (impor / lokal)
        return asal;
    }

    public String getRating() { //getter buat rating movie nya (umum/remaja/dewasa)
        return rating;
    }
}
