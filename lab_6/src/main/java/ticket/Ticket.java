package ticket;

import movie.Movie;

public class Ticket {

    private Movie film; //objek movie yang nama variablenya Film
    private String jadwal;
    private boolean is3D; //kalo is3D nya true berarti filmnya 3 dimensi, kalo engga berarti film Biasa

    public Ticket(Movie film, String jadwal, boolean is3D) { //constructor
        this.film = film;
        this.jadwal = jadwal;
        this.is3D = is3D;
    }

    public boolean isIs3D() { //getter is3D
        return is3D;
    }

    public Movie getFilm() { //getter Movie, return nya dalam bentuk sebuah objek Movie
        return film;
    }

    public String getJadwal() { //getter jadwal filmnya
        return jadwal;
    }
}
