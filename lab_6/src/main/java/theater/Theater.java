package theater;

import movie.Movie;
import ticket.Ticket;

import java.util.ArrayList;

public class Theater {

    private String namaTheater; //Nama Theater nya
    private int saldo = 0; //Saldo Kas dari Theater tersebut, defaultnya 0 kalo ga di assign
    private ArrayList<Ticket> ticket2nya; //ArrayList yang isinya Object Ticket
    private Movie[] film2nya; //Array yang berisi object/class Movie

    public Theater(String namaTheater, int saldo, ArrayList<Ticket> ticket2nya, Movie[] film2nya) {
        this.namaTheater = namaTheater;
        this.saldo = saldo;
        this.ticket2nya = ticket2nya;
        this.film2nya = film2nya;
    }

    public ArrayList<Ticket> getTicket2nya() { //getter untuk mengambil arrayList Ticket2nya
        return ticket2nya;
    }

    public int getSaldo() { //getter untuk mengambil saldo
        return saldo;
    }

    public Movie[] getFilm2nya() { //getter array Film2nya
        return film2nya;
    }

    public String getNamaTheater() { //getter nama Theaternya
        return namaTheater;
    }

    public void updateSaldo(int saldo) { //Method update untuk menambah Saldo theater sesuai parameternya (jumlah yg ingin ditambah)
        this.saldo += saldo;
    }

    public void printInfo() { //METHOD untuk print info dari Theaternya
        System.out.println("------------------------------------------------------------------");
        System.out.println("Bioskop                 : " + this.namaTheater);
        System.out.println("Saldo Kas               : " + this.saldo);
        System.out.println("Jumlah tiket tersedia   : " + ticket2nya.size());
        System.out.print("Daftar Film tersedia    : "+ film2nya[0].getJudul()); //ngambil film.getJudul index ke 0
        if(film2nya.length > 0) { //kalo Array film2nya gak kosong
            for (int i = 1; i<film2nya.length; i++) {   //ngeloop film indeks ke 1 sampe akhir
                System.out.print(", " + film2nya[i].getJudul()); //mengambil judul dari object Film indeks ke i
            }
        }
        System.out.println();
        System.out.println("------------------------------------------------------------------");
    }

    public static void printTotalRevenueEarned(Theater[] theaters) { //Method untuk ngeprint total Kas, parameternya array isinya semua theater yg ada
        int jumlahPendapatanKohMas = 0; //jumlah pendapatan seluruh theater
        for (Theater th : theaters) { //ngeloop untuk setiap theater
            jumlahPendapatanKohMas += th.getSaldo(); //total pendapatan koh mas ditambah dengan saldo setiap theater
        }
        System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + jumlahPendapatanKohMas);
        for(Theater t : theaters) { //ngeloop lagi semua theater
            System.out.println("------------------------------------------------------------------");
            System.out.println("Bioskop         : "+t.getNamaTheater());
            System.out.println("Saldo Kas       : "+t.getSaldo());
            System.out.println("------------------------------------------------------------------");
        }
    }
}
