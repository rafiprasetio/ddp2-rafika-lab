package customer;

import movie.Movie;
import theater.Theater;
import ticket.Ticket;

public class Customer {
    private String nama; //nama Customer
    private boolean isCewe; //boolean apakah dia Cewe atau bukan
    private int umur; //integer umur Customer

    public Customer(String nama, boolean isCewe, int umur) { //constructor
        this.isCewe = isCewe;
        this.nama = nama;
        this.umur = umur;
    }

    public boolean isCewe() { //getter boolean isCewe
        return isCewe;
    }

    public int getUmur() { //getter umurnya
        return umur;
    }

    public String getNama() { //getter untuk nama nya
        return nama;
    }
    //method orderTicket untuk seorang customer memesan ticket
    public Ticket orderTicket(Theater theater, String judulFilm, String jadwal, String jenis) { //parameternya sesuai soal
        int hargaTiketBiasa = 60000; //variable harga tiket film biasa
        int hargaTiket3D = 72000; //variable harga tiket film 3 dimensi (nambah 20%)
        Ticket tiket = null; //object ticket untuk di return pas looping
        boolean hariTersedia = false; //variable jika film tersedia pada hari sesuai input maka akan jadi true
        if(jadwal.equalsIgnoreCase("Sabtu") || jadwal.equalsIgnoreCase("Minggu")) { //cek apakah jadwal film yang akan dibeli Sabtu / Minggu
            hargaTiketBiasa = 100000; //sabtu minggu harga naik
            hargaTiket3D = 120000; //sabtu minggu harga naik
        }
        for(Ticket tix : theater.getTicket2nya()) { //ngeloop setiap ticket yang ada di theater
            if(tix.getFilm().getJudul().equalsIgnoreCase(judulFilm) && tix.getJadwal().equalsIgnoreCase(jadwal)) { //cek judul film yang lagi kita loop sama gak kayak inputannya, dan jadwalnya juga
                hariTersedia = true; //kalo jadwalnya sama berarti hariTersedia nya kita jadiin true
                if(jenis.equalsIgnoreCase("3 dimensi") && tix.isIs3D()) { //dicek apakah tiket inputan/pesanan nya 3 dimensi atau bukan, dan di cek di tiket yg sdh ada filmnya 3D atau engga
                    if(tix.getFilm().getRating().equalsIgnoreCase("Umum")) { //kalo filmnya rating "UMUM" bisa ditonton semua kalangan
                        System.out.println(this.nama + " telah membeli tiket " + judulFilm + " jenis 3 Dimensi di " + theater.getNamaTheater() + " pada hari "
                                + jadwal + " seharga Rp. " + hargaTiket3D);
                        theater.updateSaldo(hargaTiket3D); //saldo theater nya nambah kalo ada yang beli tiket
                        tiket = tix; //tiket yang bakal di return bakal jadi si 'tix', tix itu yang kita loop sekarang (baca line 39)
                    }
                    else if(tix.getFilm().getRating().equalsIgnoreCase("Remaja")) { //kalo ratingnya remaja
                        if(13 <= this.umur) { //cek umurnya diatas 13 atau engga
                            System.out.println(this.nama + " telah membeli tiket " + judulFilm + " jenis 3 Dimensi di " + theater.getNamaTheater() + " pada hari "
                                    + jadwal + " seharga Rp. " + hargaTiket3D);
                            theater.updateSaldo(hargaTiket3D); //update saldo theaternya
                            tiket = tix; //tiket yang bakal di return bakal jadi si 'tix', tix itu yang kita loop sekarang (baca line 39)
                        }
                        else { //kalo umurnya dibawah 13 tahun belum cukup umur
                            System.out.println(this.nama +" masih belum cukup umur untuk menonton "+tix.getFilm().getJudul()+" dengan rating "+tix.getFilm().getRating());
                        }
                    }
                    else if (tix.getFilm().getRating().equalsIgnoreCase("Dewasa")) { //kalo ratingnya Dewasa
                        if (17 < this.umur) { //cek umurnya diatas 17 ga
                            System.out.println(this.nama + " telah membeli tiket " + judulFilm + " jenis 3 Dimensi di " + theater.getNamaTheater() + " pada hari "
                                    + jadwal + " seharga Rp. " + hargaTiket3D);
                            theater.updateSaldo(hargaTiket3D); //update saldo theaternya
                            tiket = tix; //tiket yang bakal di return bakal jadi si 'tix', tix itu yang kita loop sekarang (baca line 39)
                        }
                        else { //kalo belum diatas 17 tahun berarti belum cukup umur
                            System.out.println(this.nama +" masih belum cukup umur untuk menonton "+tix.getFilm().getJudul()+" dengan rating "+tix.getFilm().getRating());
                        }
                    }
                }
                else if (jenis.equalsIgnoreCase("Biasa") && !tix.isIs3D()) { //cek apakah tiket yang di input/pesen genre nya Biasa (tidak 3D) dan tiket yang tersedia versi Biasa atau engga
                    if(tix.getFilm().getRating().equalsIgnoreCase("Umum")) {
                        System.out.println(this.nama + " telah membeli tiket " + judulFilm + " jenis Biasa di " + theater.getNamaTheater() + " pada hari "
                                + jadwal + " seharga Rp. " + hargaTiketBiasa);
                        theater.updateSaldo(hargaTiketBiasa);
                        tiket = tix;
                    }
                    else if(tix.getFilm().getRating().equalsIgnoreCase("Remaja")) {
                        if(13 <= this.umur) {
                            System.out.println(this.nama + " telah membeli tiket " + judulFilm + " jenis Biasa di " + theater.getNamaTheater() + " pada hari "
                                    + jadwal + " seharga Rp. " + hargaTiketBiasa);
                            theater.updateSaldo(hargaTiketBiasa);
                            tiket = tix;
                        }
                        else {
                            System.out.println(this.nama +" masih belum cukup umur untuk menonton "+tix.getFilm().getJudul()+" dengan rating "+tix.getFilm().getRating());
                        }
                    }
                    else if (tix.getFilm().getRating().equalsIgnoreCase("Dewasa")) {
                        if (17 < this.umur) {
                            System.out.println(this.nama + " telah membeli tiket " + judulFilm + " jenis Biasa di " + theater.getNamaTheater() + " pada hari "
                                    + jadwal + " seharga Rp. " + hargaTiketBiasa);
                            theater.updateSaldo(hargaTiketBiasa);
                            tiket = tix;
                        }
                        else {
                            System.out.println(this.nama +" masih belum cukup umur untuk menonton "+tix.getFilm().getJudul()+" dengan rating "+tix.getFilm().getRating());
                        }
                    }
                }
                else { //kalo misalnya yang dipesen 3 Dimensi tapi film yang tersedia itu ga 3d alias Biasa atau sebaliknya berarti gabisa
                    System.out.println("Tiket untuk film "+ judulFilm + " jenis " + jenis + " dengan jadwal "+ jadwal + " tidak tersedia di " + theater.getNamaTheater());
                }
            }
        }
        if (!hariTersedia) { //kalo filmnya ada tapi bukan pada hari yang ada di pesanan/inputan berarti gabisa juga
            System.out.println("Tiket untuk film " + judulFilm + " jenis " + jenis + " dengan jadwal " + jadwal + " tidak tersedia di " + theater.getNamaTheater());
        }
        return tiket; //kembaliin object ticket
    }

    public void findMovie(Theater theater, String judul) { //method buat nyari film (parameternya theater tempat nyarinya, judul film yang dicari)
        boolean ada = false; //variable boolean ada, kalo filmnya ada nanti jadi true
        for (Movie m : theater.getFilm2nya()) { //loop object Movie yang ada di Array film2nya di Theater
            if(m.getJudul().equalsIgnoreCase(judul)) { //kalo pas di loop ternyata judul filmnya sama kayak yg mau kita cari
                ada = true; //variable boolean ada kita jadiin true
                System.out.println("------------------------------------------------------------------");
                System.out.println("Judul   : " + m.getJudul());
                System.out.println("Genre   : " + m.getGenre());
                System.out.println("Durasi  : " + m.getMenit() + " menit");
                System.out.println("Rating  : " + m.getRating());
                System.out.println("Jenis   : Film " + m.getAsal());
                System.out.println("------------------------------------------------------------------");
            }
        }
        if(!ada) { //kalo pas udah ngeloop semua taunya variable 'ada' nya masih false (berarti filmnya gaada)
            System.out.println("Film " + judul +" yang dicari "+this.nama+" tidak ada di bioskop "+theater.getNamaTheater());
        }
    }
}
